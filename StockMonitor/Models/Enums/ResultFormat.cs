﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace StockMonitor.Models.Enums
{
    public enum ResultFormat
    {
        html,
        csv,
        pdf
    }
}
