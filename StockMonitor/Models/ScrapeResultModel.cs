﻿namespace StockMonitor.Models;

public class ScrapeResultModel
{
    public List<StockModel> OldStocks { get; set; } = new List<StockModel>();
    public List<StockModel> CurrentStocks { get; set; } = new List<StockModel>();
}
