﻿namespace StockMonitor.Models;
using CsvHelper.Configuration.Attributes;

public class StockModel
{
    [Format("yyyy-MM-dd")]
    public DateTime Date { get; set; }
    public string CompanyName { get; set; } = string.Empty;
    public string Ticker { get; set; } = string.Empty;
    public long NumberOfShares { get; set; }
    public float SharesDelta { get; set; }
    public float Weight { get; set; }

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        var otherStock = (StockModel)obj;

        return CompanyName == otherStock.CompanyName && Ticker == otherStock.Ticker;
    }

    public override int GetHashCode()
    {
        return CompanyName.GetHashCode(StringComparison.Ordinal) ^ Ticker.GetHashCode(StringComparison.Ordinal);
    }
}
