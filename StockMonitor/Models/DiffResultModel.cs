﻿namespace StockMonitor.Models;

public class DiffResultModel
{
    public List<StockModel> NewPositions { get; set; } = new List<StockModel>();
    public List<StockModel> ReducedPositions { get; set; } = new List<StockModel>();
    public List<StockModel> IncresedPositions { get; set; } = new List<StockModel>();
}
