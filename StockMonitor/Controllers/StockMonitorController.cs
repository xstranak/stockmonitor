﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using StockMonitor.Facades.Diff;
using StockMonitor.Facades.Notification;
using StockMonitor.Models.Enums;

namespace StockMonitor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StockMonitorController : ControllerBase
    {
        private readonly ILogger<StockMonitorController> _logger;
        private readonly IDiffFacade _diffFacade;
        private readonly INotificationFacade _notificationFacade;

        public StockMonitorController(ILogger<StockMonitorController> logger,
                                      IDiffFacade diffFacade, INotificationFacade notificationFacade)
        {
            _logger = logger;
            _diffFacade = diffFacade;
            _notificationFacade = notificationFacade;
        }

        [HttpGet(Name = "stockupdate-for/{mail}/{format}")]
        public async Task<ActionResult<string>> Get(string mail, ResultFormat resultFormat)
        {
            await _notificationFacade.SendEmailWithReportAsync(mail, await _diffFacade.GetDiffAsync(), resultFormat);
            Log.Information("Report was sent to {adress}", mail);
            return Ok("Report was sent to " + mail + ".");
        }
    }
}
