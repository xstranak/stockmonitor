﻿using StockMonitor.Exceptions;
using StockMonitor.Models;

namespace StockMonitor.Facades.Scraping;

/// <summary>
/// Facade for scraping and parsing stocks.
/// </summary>
public interface IScrapingFacade
{
    /// <summary>
    /// Scrapes and parses the stocks from the ARK website.
    /// </summary>
    /// <returns>The scraped and parsed stocks.</returns>
    /// <exception cref="IOException">Thrown when an error occurs while saving the stocks to a file.</exception>
    /// <exception cref="ArkCsvScrapeException">Thrown when an error occurs while scraping the CSV stock file.</exception>
    /// <exception cref="ArkCsvConverterException">Thrown when an error occurs while converting the CSV stock file.</exception>
    Task<ScrapeResultModel> ScrapeStocks();
}
