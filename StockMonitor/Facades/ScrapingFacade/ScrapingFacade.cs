﻿using Microsoft.Extensions.Options;
using StockMonitor.Configuration;
using StockMonitor.Models;
using StockMonitor.Services.CsvParsing;
using StockMonitor.Services.FileIO;
using StockMonitor.Services.Http;

namespace StockMonitor.Facades.Scraping;

public class ScrapingFacade : IScrapingFacade
{
    private readonly IHttpService _httpService;
    private readonly ICsvParsingService _csvParsingService;
    private readonly IFileIOService _fileIoService;
    private readonly string DefaultStocksFile;
    private readonly string DefaultCsvUrl;
    private readonly int DefaultRequestTimeout;

    public ScrapingFacade(IHttpService httpService, ICsvParsingService csvParsingService, IFileIOService fileIoService, IOptions<StockConfiguration> stockConfig)
    {
        _httpService = httpService;
        _csvParsingService = csvParsingService;
        _fileIoService = fileIoService;
        DefaultStocksFile = stockConfig.Value.StocksFile;
        DefaultCsvUrl = stockConfig.Value.CsvUrl;
        DefaultRequestTimeout = stockConfig.Value.RequestTimeout;
    }

    /// <inheritdoc />
    public async Task<ScrapeResultModel> ScrapeStocks()
    {
        var savedStocks = _fileIoService.LoadStockFromFile(DefaultStocksFile);

        var cancellationTokenSource = new CancellationTokenSource();
        cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(30));
        var rawCsv = await _httpService.ScrapeCsv(DefaultCsvUrl, cancellationTokenSource.Token);
        _httpService.Dispose();

        var parsedStocks = _csvParsingService.ParseCsv(rawCsv);

        if (IsSavedStocksEmpty(savedStocks) || IsNotSameDate(savedStocks, parsedStocks))
        {
            savedStocks.OldStocks = savedStocks.CurrentStocks;
            savedStocks.CurrentStocks = parsedStocks;

            _fileIoService.SaveStocksToFile(savedStocks, DefaultStocksFile);
        }

        return savedStocks;
    }

    private static bool IsSavedStocksEmpty(ScrapeResultModel savedStocks)
    {
        return savedStocks.CurrentStocks.Count == 0;
    }

    private static bool IsNotSameDate(ScrapeResultModel savedStocks, List<StockModel> parsedStocks)
    {
        return savedStocks.CurrentStocks.Count > 0 && parsedStocks.Count > 0 &&
                savedStocks.CurrentStocks[0].Date != parsedStocks[0].Date;
    }
}
