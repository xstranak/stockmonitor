﻿using StockMonitor.Exceptions;
using StockMonitor.Models;

namespace StockMonitor.Facades.Diff
{
    /// <summary>
    /// Facade for calculating differences in stock positions.
    /// </summary>
    public interface IDiffFacade
    {
        /// <summary>
        /// Calculates the difference in stock positions.
        /// </summary>
        /// <returns>The difference in stock positions.</returns>
        /// <exception cref="IOException">Thrown when an I/O error occurs while loading the JSON file with old stocks.</exception>
        /// <exception cref="ArkCsvScrapeException">Thrown when an error occurs while scraping the CSV stock file.</exception>
        /// <exception cref="ArkCsvConverterException">Thrown when an error occurs while converting the CSV stock file during parsing.</exception>
        Task<DiffResultModel> GetDiffAsync();
    }
}
