﻿using StockMonitor.Facades.Scraping;
using StockMonitor.Models;
using StockMonitor.Services.Diff;

namespace StockMonitor.Facades.Diff
{
    public class DiffFacade : IDiffFacade
    {
        private readonly IDiffService _diffService;
        private readonly IScrapingFacade _scrapingFacade;

        public DiffFacade(IDiffService diffService, IScrapingFacade scrapingFacade)
        {
            _diffService = diffService;
            _scrapingFacade = scrapingFacade;
        }

        /// <inheritdoc />
        public async Task<DiffResultModel> GetDiffAsync()
        {
            return _diffService.CalculateDiff(await _scrapingFacade.ScrapeStocks());
        }
    }
}
