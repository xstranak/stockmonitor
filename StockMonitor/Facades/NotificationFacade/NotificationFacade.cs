﻿using StockMonitor.Models;
using StockMonitor.Models.Enums;
using StockMonitor.Services.Notification;
using StockMonitor.Services.Translator;

namespace StockMonitor.Facades.Notification
{
    public class NotificationFacade : INotificationFacade
    {
        private readonly INotificationService _notificationService;
        private readonly ITranslatorService _translatorService;

        public NotificationFacade(INotificationService notificationService, ITranslatorService translatorService)
        {
            _notificationService = notificationService;
            _translatorService = translatorService;
        }

        /// <inheritdoc />
        public async Task SendEmailWithReportAsync(string emailAddress, DiffResultModel diffResult, ResultFormat format)
        {
            var fileName = "report." + format.ToString();
            var file = _translatorService.CreateFile(diffResult, format, fileName);

            await _notificationService.SendEmail(emailAddress, file, format);
        }
    }
}

