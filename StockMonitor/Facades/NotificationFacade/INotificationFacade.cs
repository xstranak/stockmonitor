﻿using StockMonitor.Models;
using StockMonitor.Models.Enums;

namespace StockMonitor.Facades.Notification
{
    /// <summary>
    ///  Facade for sending email with diff results.
    /// </summary>
    public interface INotificationFacade
    {
        // possibility to change to not only mail but other (by enum or new facade)
        /// <summary>
        /// Sends an email with the diff results as a CSV attachment.
        /// </summary>
        /// <exception cref="FormatException"></exception>
        /// <exception cref="NotificationFailedException"></exception>
        Task SendEmailWithReportAsync(string emailAddress, DiffResultModel diffResult, ResultFormat format);
    }
}
