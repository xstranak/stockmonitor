﻿using System.Configuration;
using System.Text.Json.Serialization;
using Serilog;
using StockMonitor.Configuration;
using StockMonitor.DI;
using StockMonitor.Exceptions.ExceptionFilter;

var builder = WebApplication.CreateBuilder(args);

// Register Facades and Services for controllers
builder.Services.RegisterBLDependencies();

// Add services to the container.
builder.Services.AddControllers(options =>
{
    options.Filters.Add(new ControllerExceptionFilter());
}).AddJsonOptions(options =>
{
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
}); ;

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


// configure logging
Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(builder.Configuration)
                .CreateLogger();

builder.Services.Configure<EmailConfiguration>(builder.Configuration.GetSection("EmailConfiguration"));
builder.Services.Configure<StockConfiguration>(builder.Configuration.GetSection("StockConfiguration"));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
