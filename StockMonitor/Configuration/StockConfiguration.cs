﻿namespace StockMonitor.Configuration
{
    public class StockConfiguration
    {
        public string StocksFile { get; set; } = "stocks.json";

        public string CsvUrl { get; set; } = "https://ark-funds.com/wp-content/uploads/funds-etf-csv/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv";
        public int RequestTimeout { get; set; } = 30;
    }
}
