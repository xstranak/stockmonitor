﻿namespace StockMonitor.Exceptions;

public class ArkCsvScrapeException : Exception
{
    public ArkCsvScrapeException()
    {
    }

    public ArkCsvScrapeException(string message)
        : base(message)
    {
    }

    public ArkCsvScrapeException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
