﻿namespace StockMonitor.Exceptions;

public class ArkCsvConverterException : Exception
{
    public ArkCsvConverterException()
    {
    }

    public ArkCsvConverterException(string message)
        : base(message)
    {
    }

    public ArkCsvConverterException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
