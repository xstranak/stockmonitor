﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace StockMonitor.Exceptions.ExceptionFilter
{
    public class ControllerExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError; // Default to 500 if not specified
            var response = "An unexpected error occurred";

            switch (context.Exception)
            {
                case ArkCsvConverterException:
                case ArkCsvScrapeException:
                    status = HttpStatusCode.BadRequest; // 400 Bad Request
                    response = context.Exception.Message;
                    break;
                case NotificationFailedException:
                    status = HttpStatusCode.ServiceUnavailable; // 503 Service Unavailable
                    response = context.Exception.Message;
                    break;
                default:
                    break;
            }

            context.Result = new ObjectResult(response) { StatusCode = (int)status };
            context.ExceptionHandled = true;
        }
    }
}
