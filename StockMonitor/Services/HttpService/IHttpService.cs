﻿namespace StockMonitor.Services.Http;

public interface IHttpService : IDisposable
{
    Task<string> ScrapeCsv(string csvUrl, CancellationToken cancellationToken);
}
