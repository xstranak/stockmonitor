﻿using StockMonitor.Exceptions;

namespace StockMonitor.Services.Http;

public class HttpService : IHttpService, IDisposable
{
    private readonly HttpClient _client;

    public HttpService()
    {
        _client = new HttpClient();
        _client.DefaultRequestHeaders.Add("accept-encoding", "gzip, deflate, br, zstd");
        _client.DefaultRequestHeaders.Add("user-agent",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) " +
            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36");
    }

    public async Task<string> ScrapeCsv(string csvUrl, CancellationToken cancellationToken)
    {
        try
        {
            var response = await _client.GetAsync(csvUrl, cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                switch (response.StatusCode)
                {
                    case System.Net.HttpStatusCode.NotFound:
                        throw new ArkCsvScrapeException("CSV data not found at URL: " + csvUrl + ". Status code: " + response.StatusCode);
                    case System.Net.HttpStatusCode.Forbidden:
                        throw new ArkCsvScrapeException($"Access to CSV data is forbidden at URL: " + csvUrl + ". Status code: " + response.StatusCode);
                    default:
                        throw new ArkCsvScrapeException($"Failed to scrape CSV at URL: " + csvUrl + ". Status code: " + response.StatusCode);
                }
            }
            return await response.Content.ReadAsStringAsync(cancellationToken);
        }
        catch (HttpRequestException e)
        {
            throw new ArkCsvScrapeException($"Failed to scrape CSV from URL: " + csvUrl, e);
        }
        catch (TaskCanceledException e)
        {
            throw new ArkCsvScrapeException("Scrape CSV task was canceled", e);
        }
    }

    public void Dispose()
    {
        _client.Dispose();
    }
}
