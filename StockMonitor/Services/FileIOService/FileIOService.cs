﻿using System.Text.Json;
using StockMonitor.Models;

namespace StockMonitor.Services.FileIO;

public class FileIOService : IFileIOService
{
    public ScrapeResultModel LoadStockFromFile(string stocksFileName)
    {
        if (File.Exists(stocksFileName))
        {
            var json = File.ReadAllText(stocksFileName);
            var stocks = JsonSerializer.Deserialize<ScrapeResultModel>(json);

            if (stocks != null)
            {
                return stocks;
            }
        }

        return new ScrapeResultModel();
    }

    public void SaveStocksToFile(ScrapeResultModel stocks, string stocksFileName)
    {
        var json = JsonSerializer.Serialize(stocks, new JsonSerializerOptions
        {
            WriteIndented = true
        });
        try
        {
            File.WriteAllTextAsync(stocksFileName, json);
        }
        catch (IOException e)
        {
            throw new IOException($"Failed to save JSON file.", e);
        }
    }
}
