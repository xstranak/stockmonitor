﻿using StockMonitor.Models;

namespace StockMonitor.Services.FileIO;

public interface IFileIOService
{
    ScrapeResultModel LoadStockFromFile(string stocksFileName);
    void SaveStocksToFile(ScrapeResultModel stocks, string stocksFileName);
}
