﻿using StockMonitor.Models;
using StockMonitor.Models.Enums;

namespace StockMonitor.Services.Translator
{
    public interface ITranslatorService
    {
        byte[] CreateFile(DiffResultModel model, ResultFormat format, string filename = "");
    }
}
