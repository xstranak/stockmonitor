﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using CsvHelper;
using iText.Html2pdf;
using iText.Kernel.Pdf;
using Serilog;
using StockMonitor.Models;
using StockMonitor.Models.Enums;

namespace StockMonitor.Services.Translator
{
    public class TranslatorService : ITranslatorService
    {
        public byte[] CreateFile(DiffResultModel model, ResultFormat format, string filename = "")
        {
            switch (format)
            {
                case ResultFormat.csv:
                    return CreateCSV(model, filename);
                case ResultFormat.html:
                    return CreateHTML(model);
                case ResultFormat.pdf:
                    return CreatePDF(model);
                default:
                    Log.Error("Wrong argument in CreateFile: " + format.ToString());
                    throw new ArgumentException("Invalid file format: " + format.ToString());
            }
        }
        private byte[] CreateCSV(DiffResultModel model, string filename)
        {
            try
            {
                Log.Information("Creating a csv file.");
                using (var memoryStream = new MemoryStream())
                using (var writer = new StreamWriter(memoryStream))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    WriteSection(csv, "IncresedPositions", model.IncresedPositions, true);
                    WriteSection(csv, "NewPositions", model.NewPositions);
                    WriteSection(csv, "ReducedPositions", model.ReducedPositions);
                    writer.Flush();
                    memoryStream.Position = 0;

                    return memoryStream.ToArray();

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Creation of csv file failed");
                throw new FormatException("An error occurred while creating the CSV file.", ex);
            }
        }

        private static void WriteSection(CsvWriter csv, string sectionHeader, IEnumerable<StockModel> positions, bool writeHeader = false)
        {
            if (writeHeader)
            {
                csv.WriteHeader<StockModel>();
                csv.NextRecord();
            }

            // Write section name
            csv.WriteComment(sectionHeader);

            // Write a new line
            csv.NextRecord();

            // Write records
            csv.WriteRecords(positions);
        }


        private byte[] CreatePDF(DiffResultModel model)
        {
            string htmlContent = CreateHTMLString(model);

            try
            {
                Log.Information("Creating a pdf file.");


                using (var ms = new MemoryStream())
                {
                    using (var pdfWriter = new PdfWriter(ms))
                    {
                        using (var htmlStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(htmlContent)))
                        {
                            HtmlConverter.ConvertToPdf(htmlStream, pdfWriter);
                        }
                    }

                    return ms.ToArray();
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Creation of pdf file failed");
                throw new FormatException("An error occurred while creating the pdf file.", ex);
            }
        }

        private byte[] CreateHTML(DiffResultModel model)
        {
            var htmlString = CreateHTMLString(model);
            return System.Text.Encoding.UTF8.GetBytes(htmlString);
        }

        private string CreateHTMLString(DiffResultModel model)
        {
            try
            {
                Log.Information("Creating an HTML table.");
                var html = new StringBuilder();

                WriteHTMLSection(html, "IncresedPositions", model.IncresedPositions);
                WriteHTMLSection(html, "NewPositions", model.NewPositions);
                WriteHTMLSection(html, "ReducedPositions", model.ReducedPositions);

                return html.ToString();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Creation of HTML table failed");
                throw new FormatException("An error occurred while creating the HTML table.", ex);
            }
        }

        private static void WriteHTMLSection(StringBuilder html, string sectionHeader, IEnumerable<StockModel> positions)
        {
            // Write section name
            html.AppendFormat(CultureInfo.InvariantCulture, "<h2>{0}</h2>", sectionHeader);

            // Start table
            html.Append("<table>");

            // Write headers
            WriteTableHeaders(html);

            // Write records
            foreach (var position in positions)
            {
                WriteTableRow(html, position);
            }

            // End table
            html.Append("</table>");
        }

        private static void WriteTableRow(StringBuilder html, StockModel position)
        {
            html.Append("<tr>");
            foreach (var prop in typeof(StockModel).GetProperties())
            {
                html.AppendFormat(CultureInfo.InvariantCulture, "<td>{0}</td>", prop.GetValue(position));
            }
            html.Append("</tr>");
        }

        private static void WriteTableHeaders(StringBuilder html)
        {
            html.Append("<tr>");
            foreach (var prop in typeof(StockModel).GetProperties())
            {
                html.AppendFormat(CultureInfo.InvariantCulture, "<th>{0}</th>", prop.Name);
            }
            html.Append("</tr>");
        }
    }



}
