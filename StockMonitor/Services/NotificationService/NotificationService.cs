﻿using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Options;
using Serilog;
using StockMonitor.Configuration;
using StockMonitor.Models.Enums;
using static Org.BouncyCastle.Bcpg.Attr.ImageAttrib;

namespace StockMonitor.Services.Notification
{

    public class NotificationService : INotificationService
    {
        private readonly EmailConfiguration _emailConfig;

        public NotificationService(IOptions<EmailConfiguration> emailConfig)
        {
            _emailConfig = emailConfig.Value;
        }

        public async Task SendEmail(string emailAddress, byte[] bytes, ResultFormat format)
        {
            var message = CreateEmailMessage(emailAddress, bytes, format);
            using var smtpClient = new SmtpClient(_emailConfig.SmtpClient);

            SetupSmptClient(smtpClient);

            try
            {
                Log.Information("SMTP sending email to {address}", emailAddress);
                await smtpClient.SendMailAsync(message);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "NotificationSendingFailedException");
                throw new NotificationFailedException("Failed to send email", ex);
            }
        }

        #region Private

        private SmtpClient SetupSmptClient(SmtpClient smtpClient)
        {
            smtpClient.Port = _emailConfig.SmtpPort;
            smtpClient.EnableSsl = _emailConfig.UseSsl;

            smtpClient.Credentials = new NetworkCredential(_emailConfig.Email, _emailConfig.Password);

            return smtpClient;
        }

        private MailMessage CreateEmailMessage(string emailAddress, byte[] bytes, ResultFormat format)
        {
            EnsureEmailIsValid(emailAddress);
            EnsureDataAreValid(bytes);

            var message = CreateMessage(emailAddress);

            if (format == ResultFormat.html)
            {
                message.IsBodyHtml = true;
                ModifyMessageBody(message, bytes);
            }
            else
            {
                AddAttachmentToMessage(message, bytes, format);
            }

            return message;
        }

        private MailMessage CreateMessage(string emailAddress)
        {
            var message = new MailMessage();

            message.From = new MailAddress(_emailConfig.Email);
            message.To.Add(emailAddress);
            message.Subject = "Daily Stock Price Report";
            message.Body = "Dear Valued Customer,\r\n" +
                            "Please find attached the daily report detailing stock price movements. This report will help you stay informed about market trends and make informed investment decisions.\r\n" +
                            "Feel free to reach out if you have any questions.\r\n" +
                            "Best regards, ARK Invest Team \r\n";

            return message;
        }

        private void ModifyMessageBody(MailMessage message, byte[] bytes)
        {
            var body = new StringBuilder(message.Body);

            body.AppendLine("");
            body.Append(Encoding.ASCII.GetString(bytes));

            message.Body = body.ToString();
        }

        private void AddAttachmentToMessage(MailMessage message, byte[] bytes, ResultFormat format)
        {
            using (var stream = new MemoryStream(bytes))
            {
                var fileName = _emailConfig.ReportName + "." + format.ToString();
                var attachment = new Attachment(stream, fileName, GetMimeType(format));
                message.Attachments.Add(attachment);
            }
        }

        public string GetMimeType(ResultFormat format)
        {
            switch (format)
            {
                case ResultFormat.csv:
                    return "text/csv";
                case ResultFormat.pdf:
                    return "application/pdf";
                default:
                    throw new ArgumentOutOfRangeException(nameof(format), format, null);
            }
        }

        private static void EnsureEmailIsValid(string emailAddress)
        {
            var pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";
            var regex = new Regex(pattern);

            if (emailAddress.Length == 0 || !regex.IsMatch(emailAddress))
            {
                Log.Error("NotificationFailedException. There is an issue with email: {emailAddress}", emailAddress);
                throw new NotificationFailedException("There is an issue with email: " + emailAddress + ".");
            }
        }

        private static void EnsureDataAreValid(byte[] bytes)
        {
            if (bytes.Length == 0)
            {
                Log.Error("NotificationFailedException - Provided file of report is empty");
                throw new NotificationFailedException("Provided file of report is empty.");
            }
        }

        #endregion Private
    }
}
