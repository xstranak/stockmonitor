﻿using StockMonitor.Models.Enums;

namespace StockMonitor.Services.Notification
{
    public interface INotificationService
    {
        Task SendEmail(string emailAddress, byte[] bytes, ResultFormat format);
    }
}
