﻿using StockMonitor.Models;

namespace StockMonitor.Services.Diff
{
    public interface IDiffService
    {
        DiffResultModel CalculateDiff(ScrapeResultModel scrapedStocks);
    }
}

