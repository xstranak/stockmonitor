﻿using StockMonitor.Models;

namespace StockMonitor.Services.Diff
{
    public class DiffService : IDiffService
    {
        public DiffResultModel CalculateDiff(ScrapeResultModel scrapedStocks)
        {
            var diffResult = new DiffResultModel();

            foreach (var oldStock in scrapedStocks.OldStocks)
            {
                var newStock = scrapedStocks.CurrentStocks.FirstOrDefault(stock => stock.Equals(oldStock));
                if (newStock == null)
                {
                    diffResult.ReducedPositions.Add(new StockModel
                    {
                        CompanyName = oldStock.CompanyName,
                        Ticker = oldStock.Ticker,
                        NumberOfShares = 0,
                        SharesDelta = -100,
                        Weight = 0
                    });
                }
                else
                {
                    CalculateStockChange(oldStock, newStock, diffResult);
                }
            }

            diffResult.NewPositions = scrapedStocks.CurrentStocks.Except(scrapedStocks.OldStocks).ToList();

            return diffResult;
        }

        private static void CalculateStockChange(StockModel oldStock, StockModel newStock, DiffResultModel diffResult)
        {
            var sharesDelta = CalculateDelta(newStock.NumberOfShares, oldStock.NumberOfShares);
            if (sharesDelta == null)
            {
                return;
            }

            var stock = new StockModel
            {
                CompanyName = newStock.CompanyName,
                Ticker = newStock.Ticker,
                NumberOfShares = newStock.NumberOfShares,
                SharesDelta = (float)sharesDelta,
                Weight = newStock.Weight
            };

            if (stock.SharesDelta > 0)
            {
                diffResult.IncresedPositions.Add(stock);
            }
            else if (stock.SharesDelta < 0)
            {
                diffResult.ReducedPositions.Add(stock);
            }
        }

        private static float? CalculateDelta(long newValue, long oldValue)
        {
            if (oldValue == 0)
            {
                return null;
            }

            float diffShares = newValue - oldValue;
            var percentageChange = diffShares / oldValue * 100;

            return (float)Math.Round(percentageChange, 2);
        }
    }
}
