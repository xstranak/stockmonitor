﻿using StockMonitor.Models;

namespace StockMonitor.Services.CsvParsing;

public interface ICsvParsingService
{
    List<StockModel> ParseCsv(string rawCsv);
}
