﻿using System.Globalization;
using CsvHelper;
using CsvHelper.TypeConversion;
using StockMonitor.Exceptions;
using StockMonitor.Models;

namespace StockMonitor.Services.CsvParsing;

public class CsvParsingService : ICsvParsingService
{
    public List<StockModel> ParseCsv(string rawCsv)
    {
        using var csvReader = new CsvReader(new StringReader(rawCsv), CultureInfo.InvariantCulture);
        csvReader.Read();
        csvReader.ReadHeader();

        var stocks = new List<StockModel>();
        while (csvReader.Read())
        {
            if (csvReader.GetField("date").StartsWith("Investors should carefully ", StringComparison.InvariantCulture))
            {
                continue;
            }
            try
            {
                StockModel stock = new()
                {
                    Date = csvReader.GetField<DateTime>("date"),
                    CompanyName = csvReader.GetField<string>("company"),
                    Ticker = csvReader.GetField<string>("ticker"),
                    NumberOfShares = long.Parse(csvReader.GetField<string>("shares").Replace(",", "", StringComparison.InvariantCulture), CultureInfo.InvariantCulture),
                    Weight = float.Parse(csvReader.GetField<string>("weight (%)").Replace("%", "", StringComparison.InvariantCulture), CultureInfo.InvariantCulture)
                };
                stocks.Add(stock);
            }
            catch (TypeConverterException e)
            {
                throw new ArkCsvConverterException($"Failed to scrape CSV", e);
            }
        }
        return stocks;
    }
}
