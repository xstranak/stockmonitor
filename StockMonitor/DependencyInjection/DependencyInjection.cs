﻿using StockMonitor.Facades.Diff;
using StockMonitor.Facades.Notification;
using StockMonitor.Facades.Scraping;
using StockMonitor.Services.CsvParsing;
using StockMonitor.Services.Diff;
using StockMonitor.Services.FileIO;
using StockMonitor.Services.Http;
using StockMonitor.Services.Notification;
using StockMonitor.Services.Translator;

namespace StockMonitor.DI
{
    public static class StockMonitorDependencyInjection
    {
        private static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ICsvParsingService, CsvParsingService>();
            services.AddScoped<IDiffService, DiffService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<ITranslatorService, TranslatorService>();
            services.AddScoped<IFileIOService, FileIOService>();
            services.AddScoped<IHttpService, HttpService>();

        }
        private static void RegisterFacades(this IServiceCollection services)
        {
            services.AddScoped<IDiffFacade, DiffFacade>();
            services.AddScoped<INotificationFacade, NotificationFacade>();
            services.AddScoped<IScrapingFacade, ScrapingFacade>();
        }

        public static void RegisterBLDependencies(this IServiceCollection services)
        {
            RegisterFacades(services);
            RegisterServices(services);
        }
    }
}
