﻿namespace StockMonitor.Benchmarks;

using BenchmarkDotNet.Attributes;
using StockMonitor.Models;
using StockMonitor.Services.Diff;


public class DiffServiceBenchmarks
{
    private readonly DiffService _diffService;
    private readonly ScrapeResultModel _scrapeResultModel;

    public DiffServiceBenchmarks()
    {
        _diffService = new DiffService();
        _scrapeResultModel = GenerateTestScrapeResult();
    }

    [Benchmark]
    public DiffResultModel CalculateDiffBenchmark()
    {
        return _diffService.CalculateDiff(_scrapeResultModel);
    }

    private static ScrapeResultModel GenerateTestScrapeResult()
    {
        var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 1000, Weight = 0.5f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 2000, Weight = 0.8f },
                new() { CompanyName = "Company C", Ticker = "C", NumberOfShares = 500, Weight = 0.3f }
            };

        var currentStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 1200, Weight = 0.6f },
                new() { CompanyName = "Company D", Ticker = "D", NumberOfShares = 1500, Weight = 0.7f },
                new() { CompanyName = "Company E", Ticker = "E", NumberOfShares = 800, Weight = 0.4f }
            };

        return new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = currentStocks };
    }
}

