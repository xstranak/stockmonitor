﻿namespace StockMonitor.Benchmarks;

using BenchmarkDotNet.Running;

public class Program
{
    public static void Main()
    {
        var _ = BenchmarkRunner.Run<DiffServiceBenchmarks>();
    }
}
