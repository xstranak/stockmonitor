﻿using Microsoft.Extensions.Logging;
using Moq;
using StockMonitor.Controllers;
using StockMonitor.Exceptions;
using StockMonitor.Facades.Diff;
using StockMonitor.Facades.Notification;
using StockMonitor.Models;
using StockMonitor.Models.Enums;

namespace StockMonitor.Tests;

public class TestController
{
    private readonly StockMonitorController _controller;

    private readonly Mock<IDiffFacade> _diffFacadeMock = new();
    private readonly Mock<INotificationFacade> _notificationFacadeMock = new();
    private readonly Mock<ILogger<StockMonitorController>> _loggerMock = new();

    public TestController()
    {
        _controller = new StockMonitorController(
            _loggerMock.Object,
            _diffFacadeMock.Object,
            _notificationFacadeMock.Object);
    }

    public TestController StocksDiffCreated()
    {
        _diffFacadeMock.Setup(mock => mock.GetDiffAsync().Result).Returns(new DiffResultModel());
        return this;
    }

    public TestController StocksDiffNotCreated()
    {
        _diffFacadeMock.Setup(mock => mock.GetDiffAsync().Result).Throws(new ArkCsvScrapeException());
        return this;
    }

    public TestController EmailWithDiffSendWithCSVReport()
    {
        _notificationFacadeMock.Setup(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.csv))
            .Returns(Task.CompletedTask);
        return this;
    }

    public TestController EmailWithDiffNotSend()
    {
        _notificationFacadeMock.Setup(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.csv))
            .Throws(new NotificationFailedException());
        return this;
    }

    public TestController EmailWithDiffSendWithPdfReport()
    {
        _notificationFacadeMock.Setup(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.pdf))
            .Returns(Task.CompletedTask);
        return this;
    }

    public TestController EmailWithDiffSendWithHTMLReport()
    {
        _notificationFacadeMock.Setup(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.html))
            .Returns(Task.CompletedTask);
        return this;
    }

    public TestController VerifyGetStockUpdateWithCSVSuccess()
    {
        Assert.DoesNotThrowAsync(() => _controller.Get("", ResultFormat.csv));

        _diffFacadeMock.Verify(mock => mock.GetDiffAsync(), Times.Once);
        _notificationFacadeMock.Verify(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.csv), Times.Once);

        return this;
    }

    public TestController VerifyGetStockUpdateWithPdfSuccess()
    {
        Assert.DoesNotThrowAsync(() => _controller.Get("", ResultFormat.pdf));

        _diffFacadeMock.Verify(mock => mock.GetDiffAsync(), Times.Once);
        _notificationFacadeMock.Verify(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.pdf), Times.Once);

        return this;
    }

    public TestController VerifyGetStockUpdateWithHTMLSuccess()
    {
        Assert.DoesNotThrowAsync(() => _controller.Get("", ResultFormat.html));

        _diffFacadeMock.Verify(mock => mock.GetDiffAsync(), Times.Once);
        _notificationFacadeMock.Verify(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.html), Times.Once);

        return this;
    }

    public TestController VerifyGetStockUpdateFailsWhenCreatingDiff()
    {
        Assert.ThrowsAsync<ArkCsvScrapeException>(() => _controller.Get("", ResultFormat.csv));

        _diffFacadeMock.Verify(mock => mock.GetDiffAsync(), Times.Once);
        _notificationFacadeMock.Verify(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.csv), Times.Never);

        return this;
    }

    public TestController VerifyGetStockUpdateTFailsWhenSendingEmail()
    {
        Assert.ThrowsAsync<NotificationFailedException>(() => _controller.Get("", ResultFormat.csv));

        _diffFacadeMock.Verify(mock => mock.GetDiffAsync(), Times.Once);
        _notificationFacadeMock.Verify(mock => mock.SendEmailWithReportAsync(It.IsAny<string>(), It.IsAny<DiffResultModel>(), ResultFormat.csv), Times.Once);

        return this;
    }
}
