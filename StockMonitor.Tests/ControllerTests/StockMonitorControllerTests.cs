﻿namespace StockMonitor.Tests;

[TestFixture]
public class StockMonitorControllerTests
{
    [Test]
    public void GetStockUpdateWithCSVReport_ShouldSucceed_WhenDiffFacadeAndNotificationFacadeSucceed()
    {
        new TestController()
            .StocksDiffCreated()
            .EmailWithDiffSendWithCSVReport()
            .VerifyGetStockUpdateWithCSVSuccess();
    }

    [Test]
    public void GetStockUpdateWithPdfReport_ShouldSucceed_WhenDiffFacadeAndNotificationFacadeSucceed()
    {
        new TestController()
            .StocksDiffCreated()
            .EmailWithDiffSendWithPdfReport()
            .VerifyGetStockUpdateWithPdfSuccess();
    }

    [Test]
    public void GetStockUpdateWithHTMLReport_ShouldSucceed_WhenDiffFacadeAndNotificationFacadeSucceed()
    {
        new TestController()
            .StocksDiffCreated()
            .EmailWithDiffSendWithHTMLReport()
            .VerifyGetStockUpdateWithHTMLSuccess();
    }

    [Test]
    public void GetStockUpdate_ShouldFail_WhenDiffFacadeThrowsException()
    {
        new TestController()
            .StocksDiffNotCreated()
            .VerifyGetStockUpdateFailsWhenCreatingDiff();
    }

    [Test]
    public void GetStockUpdate_ShouldFail_WhenNotificationFacadeThrowsException()
    {
        new TestController()
            .StocksDiffCreated()
            .EmailWithDiffNotSend()
            .VerifyGetStockUpdateTFailsWhenSendingEmail();
    }
}
