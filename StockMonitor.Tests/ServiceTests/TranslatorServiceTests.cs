﻿namespace StockMonitor.Tests;

using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using StockMonitor.Models;
using StockMonitor.Models.Enums;
using StockMonitor.Services.Translator;
using StockMonitor.Tests.MockedObjects;


[TestFixture]
public class TranslatorServiceTests
{
    private readonly MockedDependencyInjectionBuilder _injectionBuilder;
    public TranslatorServiceTests()
    {
        _injectionBuilder = MockedDependencyInjectionBuilder.CreadeDIForUnitTests();
    }

    private ServiceProvider CreateServiceProvider()
    {
        return _injectionBuilder.BuildSerivceProvider();
    }

    private DiffResultModel getModel()
    {
        var reduced = new List<StockModel>
            {
                new() { Date = new DateTime(2024, 3, 21), CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { Date = new DateTime(2024, 3, 21), CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
        var newStocks = new List<StockModel>
            {
                new() { Date = new DateTime(2024, 3, 23), CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { Date = new DateTime(2024, 3, 23), CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };

        var increased = new List<StockModel>
            {
                new() { Date = new DateTime(2024, 3, 23), CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { Date = new DateTime(2024, 3, 23), CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
        var model = new DiffResultModel { NewPositions = newStocks, IncresedPositions = increased, ReducedPositions = reduced };
        return model;

    }
    [Test]
    public void BasicTest()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var translatorService = scope.ServiceProvider.GetRequiredService<ITranslatorService>();

            DiffResultModel model = getModel();
            var fileContent = translatorService.CreateFile(model, ResultFormat.csv, "example.csv");
            string resultContent = System.Text.Encoding.UTF8.GetString(fileContent);

            var expectedContent = @"Date,CompanyName,Ticker,NumberOfShares,SharesDelta,Weight
#IncresedPositions
2024-03-23,Company A,A,100,0,10
2024-03-23,Company B,B,200,0,20
#NewPositions
2024-03-23,Company A,A,100,0,10
2024-03-23,Company B,B,200,0,20
#ReducedPositions
2024-03-21,Company A,A,100,0,10
2024-03-21,Company B,B,200,0,20
";
            // Normalize newlines
            expectedContent = expectedContent.Replace("\r\n", "\n", StringComparison.Ordinal);
            resultContent = resultContent.Replace("\r\n", "\n", StringComparison.Ordinal);
            Assert.That(resultContent, Is.EqualTo(expectedContent));
        }
    }

    [Test]
    public void EmptyTest()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var translatorService = scope.ServiceProvider.GetRequiredService<ITranslatorService>();
            var reduced = new List<StockModel>();
            var newStocks = new List<StockModel>();

            var increased = new List<StockModel>();
            var model = new DiffResultModel { NewPositions = newStocks, IncresedPositions = increased, ReducedPositions = reduced };

            var fileContent = translatorService.CreateFile(model, ResultFormat.csv, "example.csv");
            string resultContent = System.Text.Encoding.UTF8.GetString(fileContent);
            var expectedContent = @"Date,CompanyName,Ticker,NumberOfShares,SharesDelta,Weight
#IncresedPositions
#NewPositions
#ReducedPositions
";
            // Normalize newlines
            expectedContent = expectedContent.Replace("\r\n", "\n", StringComparison.Ordinal);
            resultContent = resultContent.Replace("\r\n", "\n", StringComparison.Ordinal);
            Assert.That(resultContent, Is.EqualTo(expectedContent));
        }
    }

    [Test]
    public void CreateHTML_ShouldReturnCorrectHTML()
    {
        // Arrange
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var translatorService = scope.ServiceProvider.GetRequiredService<ITranslatorService>();
            var reduced = new List<StockModel>();
            var newStocks = new List<StockModel>();

            var increased = new List<StockModel>();
            var model = new DiffResultModel { NewPositions = newStocks, IncresedPositions = increased, ReducedPositions = reduced };

            // Act
            var fileContent = translatorService.CreateFile(model, ResultFormat.html);
            string html = System.Text.Encoding.UTF8.GetString(fileContent);

            // Assert
            Assert.NotNull(html);
            Assert.IsInstanceOf<string>(html);
            Assert.IsTrue(((string)html).Contains("<table>", StringComparison.OrdinalIgnoreCase));
            Assert.IsTrue(((string)html).Contains("</table>", StringComparison.OrdinalIgnoreCase));

        }
    }

    [Test]
    public void BasicPDFTest()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var translatorService = scope.ServiceProvider.GetRequiredService<ITranslatorService>();

            var model = getModel();
            var fileContent = translatorService.CreateFile(model, ResultFormat.pdf);
            Assert.NotNull(fileContent);
        }
    }

}
