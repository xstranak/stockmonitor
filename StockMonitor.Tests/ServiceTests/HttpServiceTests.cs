﻿using Microsoft.Extensions.DependencyInjection;
using StockMonitor.Exceptions;
using StockMonitor.Services.Http;
using StockMonitor.Tests.MockedObjects;

namespace StockMonitor.Tests;

[TestFixture]
public class HttpServiceTests
{
    private readonly MockedDependencyInjectionBuilder _injectionBuilder;
    public HttpServiceTests()
    {
        _injectionBuilder = MockedDependencyInjectionBuilder.CreadeDIForUnitTests();
    }

    private ServiceProvider CreateServiceProvider()
    {
        return _injectionBuilder.BuildSerivceProvider();
    }

    [Test]
    public void ScrapeCsv_IncorrectInput_ThrowsArkCsvScrapeException()
    {
        var serviceProvider = CreateServiceProvider();
        var inputString = "https://ark-funds.com/wp-content/uploads/funds-etf-csv/NON_EXISTING_FUND_404.csv";
        using (var scope = serviceProvider.CreateScope())
        {
            var httpService = scope.ServiceProvider.GetRequiredService<IHttpService>();
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(30));
            Assert.Multiple(() =>
            {
                var ex = Assert.ThrowsAsync<ArkCsvScrapeException>(async () => await httpService.ScrapeCsv(inputString, cancellationTokenSource.Token));
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf(typeof(ArkCsvScrapeException), ex);
                Assert.True(
                    ex?.Message.StartsWith("CSV data not found at URL:", StringComparison.InvariantCulture));
            });
        }
    }
}
