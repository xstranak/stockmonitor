﻿namespace StockMonitor.Tests;

using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using StockMonitor.Models;
using StockMonitor.Services.Diff;
using StockMonitor.Tests.MockedObjects;

[TestFixture]
public class DiffServiceTests
{
    private readonly MockedDependencyInjectionBuilder _injectionBuilder;
    public DiffServiceTests()
    {
        _injectionBuilder = MockedDependencyInjectionBuilder.CreadeDIForUnitTests();
    }

    private ServiceProvider CreateServiceProvider()
    {
        return _injectionBuilder.BuildSerivceProvider();
    }

    [Test]
    public void CalculateDiff_NoChanges_ReturnsEmptyDiffResult()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();

            var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var newStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Is.Empty);
                Assert.That(diffResult.ReducedPositions, Is.Empty);
                Assert.That(diffResult.IncresedPositions, Is.Empty);
            });
        }
    }

    [Test]
    public void CalculateDiff_EmptyOldAndNewStocks_ReturnsEmptyDiffResult()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>();
            var newStocks = new List<StockModel>();
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Is.Empty);
                Assert.That(diffResult.ReducedPositions, Is.Empty);
                Assert.That(diffResult.IncresedPositions, Is.Empty);
            });
        }
    }

    [Test]
    public void CalculateDiff_OneNewPosition_ReturnsOneNewPosition()
    {

        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var newStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f },
                new() { CompanyName = "Company C", Ticker = "C", NumberOfShares = 300, Weight = 30.0f }
            };
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Has.Count.EqualTo(1));
                Assert.That(diffResult.ReducedPositions, Is.Empty);
                Assert.That(diffResult.IncresedPositions, Is.Empty);
                AssertPosition(diffResult.NewPositions[0], "Company C", "C", 300, 0f, 30.0f);
            });
        }

    }

    [Test]
    public void CalculateDiff_EmptyOldStocks_ReturnsMultipleNewPositions()
    {
        var serviceProvider = CreateServiceProvider();

        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>();
            var newStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f },
                new() { CompanyName = "Company C", Ticker = "C", NumberOfShares = 300, Weight = 30.0f },
            };
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Has.Count.EqualTo(3));
                Assert.That(diffResult.ReducedPositions, Is.Empty);
                Assert.That(diffResult.IncresedPositions, Is.Empty);
            });
        }
    }

    [Test]
    public void CalculateDiff_OnePositionIncreased_ReturnsOneIncreasedPosition()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 70, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var newStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 193, Weight = 15.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Is.Empty);
                Assert.That(diffResult.ReducedPositions, Is.Empty);
                Assert.That(diffResult.IncresedPositions, Has.Count.EqualTo(1));
                AssertPosition(diffResult.IncresedPositions[0], "Company A", "A", 193, 175.71f, 15.0f);
            });
        }
    }

    [Test]
    public void CalculateDiff_OnePositionReduced_ReturnsOneReducedPosition()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 172, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var newStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 5.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Is.Empty);
                Assert.That(diffResult.ReducedPositions, Has.Count.EqualTo(1));
                Assert.That(diffResult.IncresedPositions, Is.Empty);
                AssertPosition(diffResult.ReducedPositions[0], "Company A", "A", 100, -41.86f, 5.0f);
            });
        }
    }

    [Test]
    public void CalculateDiff_OnePositionRemoved_ReturnsOneReducedPositionWithZeroShares()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f }
            };
            var newStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f }
            };
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Is.Empty);
                Assert.That(diffResult.ReducedPositions, Has.Count.EqualTo(1));
                Assert.That(diffResult.IncresedPositions, Is.Empty);
                AssertPosition(diffResult.ReducedPositions[0], "Company B", "B", 0, -100, 0);
            });
        }
    }

    [Test]
    public void CalculateDiff_EmptyNewStocks_ReturnsMultipleReducedPositions()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f },
                new() { CompanyName = "Company C", Ticker = "C", NumberOfShares = 300, Weight = 30.0f },
            };
            var newStocks = new List<StockModel>();
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Is.Empty);
                Assert.That(diffResult.ReducedPositions, Has.Count.EqualTo(3));
                Assert.That(diffResult.IncresedPositions, Is.Empty);
            });
        }
    }

    [Test]
    public void CalculateDiff_ComplexDiff_ReturnsCorrectDiffResult()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var diffService = scope.ServiceProvider.GetRequiredService<IDiffService>();
            var oldStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 100, Weight = 10.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f },
                new() { CompanyName = "Company C", Ticker = "C", NumberOfShares = 300, Weight = 30.0f },
                new() { CompanyName = "Company D", Ticker = "D", NumberOfShares = 400, Weight = 40.0f },
            };
            var newStocks = new List<StockModel>
            {
                new() { CompanyName = "Company A", Ticker = "A", NumberOfShares = 150, Weight = 15.0f },
                new() { CompanyName = "Company B", Ticker = "B", NumberOfShares = 200, Weight = 20.0f },
                new() { CompanyName = "Company C", Ticker = "C", NumberOfShares = 250, Weight = 25.0f },
                new() { CompanyName = "Company E", Ticker = "E", NumberOfShares = 500, Weight = 50.0f },
            };
            var scrapedStocks = new ScrapeResultModel { OldStocks = oldStocks, CurrentStocks = newStocks };

            var diffResult = diffService.CalculateDiff(scrapedStocks);

            Assert.Multiple(() =>
            {
                Assert.That(diffResult.NewPositions, Has.Count.EqualTo(1));
                Assert.That(diffResult.ReducedPositions, Has.Count.EqualTo(2));
                Assert.That(diffResult.IncresedPositions, Has.Count.EqualTo(1));
            });
        }
    }

    private static void AssertPosition(
        StockModel position,
        string expectedCompanyName,
        string expectedTicker,
        int expectedNumberOfShares,
        float expectedSharesDelta,
        float expectedWeight)
    {
        Assert.Multiple(() =>
        {
            Assert.That(position.CompanyName, Is.EqualTo(expectedCompanyName), "Company name mismatch");
            Assert.That(position.Ticker, Is.EqualTo(expectedTicker), "Ticker mismatch");
            Assert.That(position.NumberOfShares, Is.EqualTo(expectedNumberOfShares), "Number of shares is not as expected");
            Assert.That(position.SharesDelta, Is.EqualTo(expectedSharesDelta), "Shares delta mismatch");
            Assert.That(position.Weight, Is.EqualTo(expectedWeight), "Weight is not as expected");
        });
    }
}
