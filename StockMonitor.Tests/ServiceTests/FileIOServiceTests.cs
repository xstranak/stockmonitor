﻿using Microsoft.Extensions.DependencyInjection;
using StockMonitor.Exceptions;
using StockMonitor.Models;
using StockMonitor.Services.FileIO;
using StockMonitor.Services.Http;
using StockMonitor.Tests.MockedObjects;

namespace StockMonitor.Tests;

[TestFixture]
public class FileIOServiceTests
{
    private readonly MockedDependencyInjectionBuilder _injectionBuilder;
    public FileIOServiceTests()
    {
        _injectionBuilder = MockedDependencyInjectionBuilder.CreadeDIForUnitTests();
    }

    private ServiceProvider CreateServiceProvider()
    {
        return _injectionBuilder.BuildSerivceProvider();
    }

    [Test]
    public void LoadStockFromFile_IncorrectFileName_ReturnsEmptyScrapeResultModel()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var fileIoService = scope.ServiceProvider.GetRequiredService<IFileIOService>();
            var result = fileIoService.LoadStockFromFile(Path.Combine("Mocks", "nonExistentJson.json"));
            Assert.Multiple(() =>
            {
                Assert.IsEmpty(result.OldStocks);
                Assert.IsEmpty(result.CurrentStocks);
            });
        }
    }

}
