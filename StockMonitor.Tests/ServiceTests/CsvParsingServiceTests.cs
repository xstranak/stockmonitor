﻿using Microsoft.Extensions.DependencyInjection;
using StockMonitor.Exceptions;
using StockMonitor.Models;
using StockMonitor.Services.CsvParsing;
using StockMonitor.Tests.MockedObjects;

namespace StockMonitor.Tests;

[TestFixture]
public class CsvParsingServiceTests
{
    private readonly MockedDependencyInjectionBuilder _injectionBuilder;
    public CsvParsingServiceTests()
    {
        _injectionBuilder = MockedDependencyInjectionBuilder.CreadeDIForUnitTests();
    }

    private ServiceProvider CreateServiceProvider()
    {
        return _injectionBuilder.BuildSerivceProvider();
    }

    [Test]
    public void ParseCsv_CorrectFormat_ReturnsListOfStocks()
    {
        var serviceProvider = CreateServiceProvider();
        var inputString =
            "date,fund,company,ticker,cusip,shares,\"market value ($)\",\"weight (%)\"\n" +
            "02/22/2022,ARKK,\"CompanyName1\",TICKER1,19260Q107,\"3,171,687\",\"$830,981,994.00\",10.52%";
        using (var scope = serviceProvider.CreateScope())
        {
            var stocks = new List<StockModel>()
            {
                new StockModel()
                {
                    Date = new DateTime(2022, 2, 22),
                    CompanyName = "CompanyName1",
                    NumberOfShares = 3171687,
                    Weight = 42,
                    Ticker = "TICKER1",
                    SharesDelta = 10.52f
                }
            };

            var csvParsingService = scope.ServiceProvider.GetRequiredService<ICsvParsingService>();
            var result = csvParsingService.ParseCsv(inputString);

            Assert.That(result, Is.EqualTo(stocks));
        }
    }

    [Test]
    public void ParseCsv_IncorrectFormat_ThrowsArkCsvConverterException()
    {
        var serviceProvider = CreateServiceProvider();
        var inputString =
            "date,fund,company,ticker,cusip,shares,\"market value ($)\",\"weight (%)\"\n" +
            "11/52/2022,ARKK,\"CompanyName1\",TICKER1,19260Q107,\"3,171,687\",\"$830,981,994.00\",10.52%";

        using (var scope = serviceProvider.CreateScope())
        {
            var csvParsingService = scope.ServiceProvider.GetRequiredService<ICsvParsingService>();

            Assert.Multiple(() =>
            {
                var ex = Assert.Throws<ArkCsvConverterException>(() => csvParsingService.ParseCsv(inputString));
                Assert.IsNotNull(ex);
                Assert.IsInstanceOf(typeof(ArkCsvConverterException), ex);
                Assert.That(ex?.Message, Is.EqualTo(
                    "Failed to scrape CSV"
                ));
            });
        }
    }
}
