﻿namespace StockMonitor.Tests;

using System.Text;
using Microsoft.Extensions.DependencyInjection;
using StockMonitor.Models.Enums;
using StockMonitor.Services.Notification;
using StockMonitor.Tests.MockedObjects;

[TestFixture]
public class NotificationServiceTests
{
    private readonly MockedDependencyInjectionBuilder _injectionBuilder;

    public NotificationServiceTests()
    {
        _injectionBuilder = MockedDependencyInjectionBuilder.CreadeDIForUnitTests();
    }

    private ServiceProvider CreateServiceProvider()
    {
        return _injectionBuilder.BuildSerivceProvider();
    }

    [Test]
    public void SendEmailWithCSV_ThrowsException_WhenEmailIsEmpty()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var notificationService = scope.ServiceProvider.GetRequiredService<INotificationService>();
            var csvBytes = new byte[] { 0x01, 0x02, 0x03 };

            var ex = Assert.ThrowsAsync<NotificationFailedException>(async () => await notificationService.SendEmail("", csvBytes, ResultFormat.csv));
            Assert.IsNotNull(ex);
            Assert.That(ex?.Message, Is.EqualTo("There is an issue with email: " + "" + "."));
        }
    }

    [Test]
    public void SendEmailWithCSV_ThrowsException_WhenEmailIsInvalid()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var notificationService = scope.ServiceProvider.GetRequiredService<INotificationService>();
            var csvBytes = new byte[] { 0x01, 0x02, 0x03 };

            var ex = Assert.ThrowsAsync<NotificationFailedException>(async () => await notificationService.SendEmail("invalid email", csvBytes, ResultFormat.csv));
            Assert.IsNotNull(ex);
            Assert.That(ex?.Message, Is.EqualTo("There is an issue with email: " + "invalid email" + "."));
        }
    }

    [Test]
    public void SendEmailWithCSV_ThrowsException_WhenCSVIsEmpty()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var notificationService = scope.ServiceProvider.GetRequiredService<INotificationService>();
            var csvBytes = Array.Empty<byte>();

            var ex = Assert.ThrowsAsync<NotificationFailedException>(async () => await notificationService.SendEmail("test@seznam.cz", csvBytes, ResultFormat.csv));
            Assert.IsNotNull(ex);
            Assert.That(ex?.Message, Is.EqualTo("Provided file of report is empty."));
        }
    }

    [Test]
    public void SendEmailWithHTML_ThrowsException_WhenEmailIsEmpty()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var notificationService = scope.ServiceProvider.GetRequiredService<INotificationService>();
            byte[] htmlBytes = Encoding.UTF8.GetBytes("test");

            var ex = Assert.ThrowsAsync<NotificationFailedException>(async () => await notificationService.SendEmail("", htmlBytes, ResultFormat.html));
            Assert.IsNotNull(ex);
            Assert.That(ex?.Message, Is.EqualTo("There is an issue with email: " + "" + "."));
        }
    }

    [Test]
    public void SendEmailWithHTML_ThrowsException_WhenEmailIsInvalid()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var notificationService = scope.ServiceProvider.GetRequiredService<INotificationService>();
            byte[] htmlBytes = Encoding.UTF8.GetBytes("test");

            var ex = Assert.ThrowsAsync<NotificationFailedException>(async () => await notificationService.SendEmail("invalid email", htmlBytes, ResultFormat.html));
            Assert.IsNotNull(ex);
            Assert.That(ex?.Message, Is.EqualTo("There is an issue with email: " + "invalid email" + "."));

        }
    }

    [Test]
    public void SendEmailWithHtml_ThrowsException_WhenHtmlIsEmpty()
    {
        var serviceProvider = CreateServiceProvider();
        using (var scope = serviceProvider.CreateScope())
        {
            var notificationService = scope.ServiceProvider.GetRequiredService<INotificationService>();
            byte[] htmlBytes = Encoding.UTF8.GetBytes("");

            var ex = Assert.ThrowsAsync<NotificationFailedException>(async () => await notificationService.SendEmail("test@seznam.cz", htmlBytes, ResultFormat.html));
            Assert.IsNotNull(ex);
            Assert.That(ex?.Message, Is.EqualTo("Provided file of report is empty."));
        }
    }
}

