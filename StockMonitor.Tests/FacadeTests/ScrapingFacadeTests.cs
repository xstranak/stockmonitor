﻿namespace StockMonitor.Tests;

[TestFixture]
public class ScrapingFacadeTests
{
    [Test]
    public async Task ScrapeStocks_ShouldSucceed_WhenScrapingServicesSucceed()
    {
        await new TestFacade()
            .PreviousStocksLoaded()
            .NewStocksDownloaded()
            .NewStocksParsed()
            .VerifyStocksScrapedSuccesfully();
    }

    [Test]
    public void ScrapeStocks_ShouldFail_WhenFileIOServiceThrowsException()
    {
        new TestFacade()
            .PreviousStocksNotLoaded()
            .VerifyScrapeStocksFailsWhenLoadingPreviousStocks();
    }

    [Test]
    public void ScrapeStocks_ShouldFail_WhenHttpServiceThrowsException()
    {
        new TestFacade()
            .PreviousStocksLoaded()
            .NewStocksNotDownloaded()
            .VerifyScrapeStocksFailsWhenDownloadingNewStocks();
    }

    [Test]
    public void ScrapeStocks_ShouldFail_WhenCsvParsingServiceThrowsException()
    {
        new TestFacade()
            .PreviousStocksLoaded()
            .NewStocksDownloaded()
            .NewStocksNotParsed()
            .VerifyScrapeStocksFailsWhenParsingNewStocks();
    }
}

