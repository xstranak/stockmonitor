﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using StockMonitor.Configuration;
using StockMonitor.Exceptions;
using StockMonitor.Facades.Diff;
using StockMonitor.Facades.Notification;
using StockMonitor.Facades.Scraping;
using StockMonitor.Models;
using StockMonitor.Models.Enums;
using StockMonitor.Services.CsvParsing;
using StockMonitor.Services.Diff;
using StockMonitor.Services.FileIO;
using StockMonitor.Services.Http;
using StockMonitor.Services.Notification;
using StockMonitor.Services.Translator;

namespace StockMonitor.Tests;

public class TestFacade
{
    private readonly ScrapingFacade _scrapingFacade;
    private readonly DiffFacade _diffFacade;
    private readonly NotificationFacade _notificationFacade;

    private readonly Mock<ICsvParsingService> _csvParsingServiceMock = new();
    private readonly Mock<IFileIOService> _fileIOServiceMock = new();
    private readonly Mock<IHttpService> _httpServiceMock = new();
    private readonly Mock<IDiffService> _diffServiceMock = new();
    private readonly Mock<ITranslatorService> _translatorServiceMock = new();
    private readonly Mock<INotificationService> _notificationServiceMock = new();
    private readonly Mock<IScrapingFacade> _scrapingFacadeMock = new();
    private readonly Mock<IOptions<StockConfiguration>> _optionsMock = new();

    public TestFacade()
    {
        var stockConfig = new StockConfiguration
        {
            StocksFile = "stocks.json",
            CsvUrl = "https://ark-funds.com/wp-content/uploads/funds-etf-csv/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv"
        };
        _optionsMock.Setup(o => o.Value).Returns(stockConfig);
        _scrapingFacade = new ScrapingFacade(_httpServiceMock.Object, _csvParsingServiceMock.Object, _fileIOServiceMock.Object, _optionsMock.Object);
        _diffFacade = new DiffFacade(_diffServiceMock.Object, _scrapingFacadeMock.Object);
        _notificationFacade = new NotificationFacade(_notificationServiceMock.Object, _translatorServiceMock.Object);
    }

    public TestFacade NewStocksParsed()
    {
        _csvParsingServiceMock.Setup(mock => mock.ParseCsv(It.IsAny<string>())).Returns(ParsedStockModels());
        return this;
    }

    public TestFacade NewStocksNotParsed()
    {
        _csvParsingServiceMock.Setup(mock => mock.ParseCsv(It.IsAny<string>())).Throws(new ArkCsvConverterException());
        return this;
    }

    public TestFacade PreviousStocksLoaded()
    {
        _fileIOServiceMock.Setup(mock => mock.LoadStockFromFile(It.IsAny<string>())).Returns(LoadedScrapeResultModel());
        return this;
    }

    public TestFacade PreviousStocksNotLoaded()
    {
        _fileIOServiceMock.Setup(mock => mock.LoadStockFromFile(It.IsAny<string>())).Throws(new IOException());
        return this;
    }

    public TestFacade NewStocksDownloaded()
    {
        _httpServiceMock.Setup(mock => mock.ScrapeCsv(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync("");
        return this;
    }

    public TestFacade NewStocksNotDownloaded()
    {
        _httpServiceMock.Setup(mock => mock.ScrapeCsv(It.IsAny<string>(), It.IsAny<CancellationToken>())).ThrowsAsync(new ArkCsvScrapeException());
        return this;
    }

    public TestFacade NewStocksDiffCalculated()
    {
        _diffServiceMock.Setup(mock => mock.CalculateDiff(It.IsAny<ScrapeResultModel>())).Returns(new DiffResultModel());
        return this;
    }

    public TestFacade StocksReportFileCreated()
    {
        _translatorServiceMock.Setup(mock => mock.CreateFile(It.IsAny<DiffResultModel>(), It.IsAny<ResultFormat>(), It.IsAny<string>())).Returns(Array.Empty<byte>());
        return this;
    }

    public TestFacade StocksReportFileNotCreated()
    {
        _translatorServiceMock.Setup(mock => mock.CreateFile(It.IsAny<DiffResultModel>(), It.IsAny<ResultFormat>(), It.IsAny<string>())).Throws(new FormatException());
        return this;
    }

    public TestFacade StocksSendViaEmailWithFile()
    {
        _notificationServiceMock.Setup(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsIn(ResultFormat.csv, ResultFormat.pdf))).Returns(Task.CompletedTask);
        return this;
    }

    public TestFacade StocksSendViaEmailWithHTML()
    {
        _notificationServiceMock.Setup(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsIn(ResultFormat.html))).Returns(Task.CompletedTask);
        return this;
    }

    public TestFacade StocksNotSendViaEmail()
    {
        _notificationServiceMock.Setup(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsAny<ResultFormat>())).Throws(new NotificationFailedException());
        _notificationServiceMock.Setup(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsIn(ResultFormat.html))).Throws(new NotificationFailedException());
        return this;
    }

    public TestFacade NewStocksScraped()
    {
        _scrapingFacadeMock.Setup(mock => mock.ScrapeStocks()).Returns(Task.FromResult(new ScrapeResultModel()));
        return this;
    }

    public TestFacade NewStocksNotSraped()
    {
        _scrapingFacadeMock.Setup(mock => mock.ScrapeStocks()).Throws(new ArkCsvScrapeException());
        return this;
    }

    public async Task<TestFacade> VerifyStocksScrapedSuccesfully()
    {
        var result = await _scrapingFacade.ScrapeStocks();

        _fileIOServiceMock.Verify(mock => mock.LoadStockFromFile(It.IsAny<string>()), Times.Once);
        _httpServiceMock.Verify(mock => mock.ScrapeCsv(It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once);
        _csvParsingServiceMock.Verify(mock => mock.ParseCsv(It.IsAny<string>()), Times.Once);

        Assert.Multiple(() =>
        {
            Assert.That(result.OldStocks[0].Date, Is.EqualTo(new DateTime(2024, 1, 2)));
            Assert.That(result.CurrentStocks[0].Date, Is.EqualTo(new DateTime(2024, 1, 3)));
        });

        return this;
    }

    public TestFacade VerifyScrapeStocksFailsWhenLoadingPreviousStocks()
    {
        Assert.ThrowsAsync<IOException>(() => _scrapingFacade.ScrapeStocks());

        _fileIOServiceMock.Verify(mock => mock.LoadStockFromFile(It.IsAny<string>()), Times.Once);
        _httpServiceMock.Verify(mock => mock.ScrapeCsv(It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Never);
        _csvParsingServiceMock.Verify(mock => mock.ParseCsv(It.IsAny<string>()), Times.Never);

        return this;
    }

    public TestFacade VerifyScrapeStocksFailsWhenDownloadingNewStocks()
    {
        Assert.ThrowsAsync<ArkCsvScrapeException>(() => _scrapingFacade.ScrapeStocks());

        _fileIOServiceMock.Verify(mock => mock.LoadStockFromFile(It.IsAny<string>()), Times.Once);
        _httpServiceMock.Verify(mock => mock.ScrapeCsv(It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once);
        _csvParsingServiceMock.Verify(mock => mock.ParseCsv(It.IsAny<string>()), Times.Never);

        return this;
    }

    public TestFacade VerifyScrapeStocksFailsWhenParsingNewStocks()
    {
        Assert.ThrowsAsync<ArkCsvConverterException>(() => _scrapingFacade.ScrapeStocks());

        _fileIOServiceMock.Verify(mock => mock.LoadStockFromFile(It.IsAny<string>()), Times.Once);
        _httpServiceMock.Verify(mock => mock.ScrapeCsv(It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once);
        _csvParsingServiceMock.Verify(mock => mock.ParseCsv(It.IsAny<string>()), Times.Once);

        return this;
    }

    public TestFacade VerifyDiffCreatedSuccesfully()
    {
        Assert.DoesNotThrowAsync(() => _diffFacade.GetDiffAsync());

        _diffServiceMock.Verify(mock => mock.CalculateDiff(It.IsAny<ScrapeResultModel>()), Times.Once);
        _scrapingFacadeMock.Verify(mock => mock.ScrapeStocks(), Times.Once);

        return this;
    }

    public TestFacade VerifyDiffFailsWhenScrapingNewStocks()
    {
        Assert.ThrowsAsync<ArkCsvScrapeException>(() => _diffFacade.GetDiffAsync());

        _scrapingFacadeMock.Verify(mock => mock.ScrapeStocks(), Times.Once);
        _diffServiceMock.Verify(mock => mock.CalculateDiff(It.IsAny<ScrapeResultModel>()), Times.Never);

        return this;
    }

    public TestFacade VerifyEmailWithCSVSendSuccesfully()
    {
        Assert.DoesNotThrowAsync(() => _notificationFacade.SendEmailWithReportAsync("", new DiffResultModel(), ResultFormat.csv));

        _translatorServiceMock.Verify(mock => mock.CreateFile(It.IsAny<DiffResultModel>(), ResultFormat.csv, It.IsAny<string>()), Times.Once);
        _notificationServiceMock.Verify(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), ResultFormat.csv), Times.Once);

        return this;
    }

    public TestFacade VerifySendEmailWithCSVFailsWhenCreatingReport()
    {
        Assert.ThrowsAsync<FormatException>(() => _notificationFacade.SendEmailWithReportAsync("", new DiffResultModel(), ResultFormat.csv));

        _translatorServiceMock.Verify(mock => mock.CreateFile(It.IsAny<DiffResultModel>(), ResultFormat.csv, It.IsAny<string>()), Times.Once);
        _notificationServiceMock.Verify(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), ResultFormat.csv), Times.Never);

        return this;
    }

    public TestFacade VerifySendEmailWithCSVFailsWhenSendingEmail()
    {
        Assert.ThrowsAsync<NotificationFailedException>(() => _notificationFacade.SendEmailWithReportAsync("", new DiffResultModel(), ResultFormat.csv));

        _translatorServiceMock.Verify(mock => mock.CreateFile(It.IsAny<DiffResultModel>(), ResultFormat.csv, It.IsAny<string>()), Times.Once);
        _notificationServiceMock.Verify(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), ResultFormat.csv), Times.Once);

        return this;
    }

    public TestFacade VerifyEmailWithPdfSendSuccesfully()
    {
        Assert.DoesNotThrowAsync(() => _notificationFacade.SendEmailWithReportAsync("", new DiffResultModel(), ResultFormat.pdf));

        _translatorServiceMock.Verify(mock => mock.CreateFile(It.IsAny<DiffResultModel>(), ResultFormat.pdf, It.IsAny<string>()), Times.Once);
        _notificationServiceMock.Verify(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), ResultFormat.pdf), Times.Once);

        return this;
    }

    public TestFacade VerifyEmailWithHTMLSendSuccesfully()
    {
        Assert.DoesNotThrowAsync(() => _notificationFacade.SendEmailWithReportAsync("", new DiffResultModel(), ResultFormat.html));

        _translatorServiceMock.Verify(mock => mock.CreateFile(It.IsAny<DiffResultModel>(), It.IsAny<ResultFormat>(), It.IsAny<string>()), Times.Once);
        _notificationServiceMock.Verify(mock => mock.SendEmail(It.IsAny<string>(), It.IsAny<byte[]>(), It.IsIn(ResultFormat.html)), Times.Once);

        return this;
    }

    private static ScrapeResultModel LoadedScrapeResultModel()
    {
        return new ScrapeResultModel
        {
            OldStocks = new List<StockModel>
            {
                new() {
                    Date = new DateTime(2024, 1, 1),
                    Ticker = "AAPL",
                    CompanyName = "Apple Inc.",
                    NumberOfShares = 120,
                    SharesDelta = 11.1f,
                    Weight = 10
                }
            },
            CurrentStocks = new List<StockModel>
            {
                new() {
                    Date = new DateTime(2024, 1, 2),
                    Ticker = "AAPL",
                    CompanyName = "Apple Inc.",
                    NumberOfShares = 122,
                    SharesDelta = 12.2f,
                    Weight = 12
                }
            }
        };
    }

    private static List<StockModel> ParsedStockModels()
    {
        return new List<StockModel>
        {
            new() {
                Date = new DateTime(2024, 1, 3),
                Ticker = "AAPL",
                CompanyName = "Apple Inc.",
                NumberOfShares = 128,
                Weight = 14
            }
        };
    }
}
