﻿namespace StockMonitor.Tests;

[TestFixture]
public class NotificationFacadeTests
{
    [Test]
    public void SendEmailWithCSVFileReportAsync_ShouldSucceed_WhenNotificationServiceAndTranslatorServiceSucceed()
    {
        new TestFacade()
            .StocksReportFileCreated()
            .StocksSendViaEmailWithFile()
            .VerifyEmailWithCSVSendSuccesfully();
    }

    [Test]
    public void SendEmailWithPdfFileReportAsync_ShouldSucceed_WhenNotificationServiceAndTranslatorServiceSucceed()
    {
        new TestFacade()
            .StocksReportFileCreated()
            .StocksSendViaEmailWithFile()
            .VerifyEmailWithPdfSendSuccesfully();
    }

    [Test]
    public void SendEmailWithHTMLReportAsync_ShouldSucceed_WhenNotificationServiceAndTranslatorServiceSucceed()
    {
        new TestFacade()
            .StocksReportFileCreated()
            .StocksSendViaEmailWithHTML()
            .VerifyEmailWithHTMLSendSuccesfully();
    }

    [Test]
    public void SendEmailWithCSVFileReportAsync_ShouldFail_WhenTranslatorServiceThrowsException()
    {
        new TestFacade()
            .StocksReportFileNotCreated()
            .VerifySendEmailWithCSVFailsWhenCreatingReport();
    }

    [Test]
    public void SendEmailWithCSVFileReportAsync_ShouldFail_WhenNotificationServiceThrowsException()
    {
        new TestFacade()
            .StocksReportFileCreated()
            .StocksNotSendViaEmail()
            .VerifySendEmailWithCSVFailsWhenSendingEmail();
    }
}

