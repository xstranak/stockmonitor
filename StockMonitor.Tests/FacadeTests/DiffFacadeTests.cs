﻿namespace StockMonitor.Tests;

[TestFixture]
public class DiffFacadeTests
{
    [Test]
    public void GetDiffAsync_ShouldSucceed_WhenDiffServiceAndScrapingFacadeSucceed()
    {
        new TestFacade()
            .NewStocksScraped()
            .NewStocksDiffCalculated()
            .VerifyDiffCreatedSuccesfully();
    }

    [Test]
    public void GetDiffAsync_ShouldFail_WhenScrapingFacadeThrowsException()
    {
        new TestFacade()
            .NewStocksNotSraped()
            .VerifyDiffFailsWhenScrapingNewStocks();
    }
}
