﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StockMonitor.Configuration;
using StockMonitor.DI;

namespace StockMonitor.Tests.MockedObjects
{
    public class MockedDependencyInjectionBuilder
    {
        private readonly IServiceCollection _serviceCollection;

        private MockedDependencyInjectionBuilder()
        {
            _serviceCollection = new ServiceCollection();
        }
        public ServiceProvider BuildSerivceProvider()
        {
            return _serviceCollection.BuildServiceProvider();
        }

        public static MockedDependencyInjectionBuilder CreadeDIForUnitTests()
        {
            var selfObject = new MockedDependencyInjectionBuilder();
            selfObject._serviceCollection.RegisterBLDependencies();

            var emailConfig = new EmailConfiguration
            {
                Email = "test@test.com",
                Password = "test",
                SmtpPort = 123,
                SmtpClient = "test.test.test",
                UseSsl = true,
                ReportName = "test.csv"
            };

            selfObject._serviceCollection.AddSingleton<IOptions<EmailConfiguration>>(new OptionsWrapper<EmailConfiguration>(emailConfig));

            return selfObject;
        }
    }
}
